//
//  Constants.swift
//  ghibli
//
//  Created by Arnold Sidiprasetija on 13/06/22.
//

import Foundation

struct Constants {
    
    struct Storyboard {
        
        static let homeViewController = "HomeVC"
    }
}
